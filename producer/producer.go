package main

import (
	"bufio"
	"log"
	"os"
	"strings"

	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect on RabbitMQ")
	defer conn.Close()

	channel, err := conn.Channel()
	failOnError(err, "Could not open Channel")
	defer channel.Close()

	queue, err := channel.QueueDeclare("hello", false, false, false, false, nil)
	failOnError(err, "Error declaring a Queue")

	reader := bufio.NewReader(os.Stdin)
	for {
		log.Printf("Send a message:")
		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\n", "", -1) // remove newline

		err = channel.Publish("", queue.Name, false, false, amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(text),
		})
		failOnError(err, "Failed to publish a message")
	}
}
